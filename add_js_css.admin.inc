<?php

/**
 * @file
 * Setting form to add js and css.
 */

define('DEFAULT_TYPE', 'external');

/**
 * Configuration form.
 */
function add_js_css_setting_form() {
  $form = array();
  $form['add_js'] = array(
    '#type' => 'textarea',
    '#title' => t('JS Links to add'),
    '#default_value' => _add_js_css_default_value(variable_get('added_js_data')),
    '#description' => t("Add JS file path, one per line. <br />Pattern will like (Js Path[Required]|Type[optional]|Scope[optional]|Weight[optional]|Url-Pattern[optional]) <br /> Like - https://maps.googleapis.com/maps/api/js | external | header |100 | node/*"),
  );

  $form['add_css'] = array(
    '#type' => 'textarea',
    '#title' => t('CSS Links to add'),
    '#default_value' => _add_js_css_default_value(variable_get('added_css_data')),
    '#description' => t("Add CSS file path, one per line. <br /> Pattern will like (Css Path[Required]|Type[optional]|Weight[optional]|Url-Pattern[optional]) <br /> Like - https://maps.googleapis.com/maps/api/css | external | 100 | node/*"),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#states' => array(
      'visible' => array(
          array(
          array(':input[name="add_js"]' => array('empty' => FALSE)),
          'or',
          array(':input[name="add_css"]' => array('empty' => FALSE)),
        ),
      ),
    ),
  );

  return $form;
}

/**
 * Implements form validation.
 */
function add_js_css_setting_form_validate($form, &$form_state) {
    $values = $form_state['values'];
    if(empty($values['add_js']) && empty($values['add_css'])) {
        form_set_error('add_js', "Both JS Links to add and CSS Links to add fields cannot be empty.");
        form_set_error('add_css');
    }
}

/**
 * Implements hook_form_submit().
 */
function add_js_css_setting_form_submit(array $form, array &$form_state) {
  if (!empty($form_state['values'])) {
    if (!empty($form_state['values']['add_js'])) {
      _add_js_css_js_handler($form_state['values']['add_js']);
    }
    if (!empty($form_state['values']['add_css'])) {
      _add_js_css_css_handler($form_state['values']['add_css']);
    }
  }
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Helper function.
 */
function _add_js_css_js_handler($js_data) {
  $js_data = preg_split("/(\r\n|\r|\n)/", $js_data, NULL, PREG_SPLIT_NO_EMPTY);
  $css_array = array();
  if (!empty($js_data)) {
    foreach ($js_data as $js_data_to_add) {
      list($filepath, $type, $scope, $weight, $url_pattern) = explode('|', $js_data_to_add);
      $filepath = trim($filepath);
      $css_array[$filepath]['type'] = trim($type);
      $css_array[$filepath]['scope'] = (!empty(trim($scope))) ? trim($scope) : DEFAULT_TYPE;
      $css_array[$filepath]['weight'] = trim($weight);
      $css_array[$filepath]['url_pattern'] = trim($url_pattern);
    }
  }
  variable_set('added_js_data', json_encode($css_array));
}

/**
 * Helper function.
 */
function _add_js_css_css_handler($css_data) {
  $css_data = preg_split("/(\r\n|\r|\n)/", $css_data, NULL, PREG_SPLIT_NO_EMPTY);
  $css_array = array();
  if (!empty($css_data)) {
    foreach ($css_data as $css_data_to_add) {
      list($filepath, $type, $weight, $url_pattern) = explode('|', $css_data_to_add);
      $filepath = trim($filepath);
      $css_array[$filepath]['type'] = trim($type);
      $css_array[$filepath]['weight'] = trim($weight);
      $css_array[$filepath]['url_pattern'] = trim($url_pattern);
    }
  }
  variable_set('added_css_data', json_encode($css_array));
}

/**
 * Helper function.
 */
function _add_js_css_default_value($default_value) {
  $default_value_array = json_decode($default_value, TRUE);
  $all_files_array = array();
  if (!empty($default_value_array)) {
    foreach ($default_value_array as $file_name => $values) {
      $file_detail = $file_name . ' | ' . implode(' | ', $values);
      $all_files_array[] = $file_detail;
    }
  }
  return implode("\n", $all_files_array);
}
